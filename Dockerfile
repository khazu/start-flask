FROM python:3.11-alpine
WORKDIR /code
ENV FLASK_APP main.py
ENV FLASK_RUN_HOST 0.0.0.0
ENV PYTHONUNBUFFERED=1
RUN apk add --no-cache gcc musl-dev linux-headers bash vim mysql-client mariadb-connector-c-dev libffi-dev build-base 

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
RUN rm requirements.txt

RUN pip install --upgrade google-cloud-storage

COPY waitress-chan /usr/bin
RUN chmod +x /usr/bin/waitress-chan
CMD ["flask", "run"]
